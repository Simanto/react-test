import React, { Component } from 'react';
import './App.css';

// Const data
const list = [
  {
    title: "React",
    url: "https://facebook.github.io/react/",
    author: "Jordan Wake",
    num_comments: 3,
    points: 4,
    objectID: 0
  },
  {
    title: "Redux",
    url: "https://github.com/reactjs/redux",
    author: "Dani Daniels, Jhony Sins",
    num_comments: 15,
    points: 21,
    objectID: 1
  }
];

// Render
class App extends Component {
  render() {
    return (
      <div className="App">
        { list.map( list => {
          return <div key={list.objectID} className={list.objectID}>
          <span><a href={list.url}>{list.title}</a></span>
          <span>{list.author}</span>
          <span>{list.num_comments}</span>
          <span>{list.points}</span>
          </div>
        })}
      </div>
    );
  }
}

export default App;
